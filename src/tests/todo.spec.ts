import * as homepage from '../infrastructure/pages/homepage.map'
import * as tasks from '../fixtures/tasks.json'
import * as tasksComponents from '../infrastructure/components/tasks.map'
import * as cyConfig from '../../config/cypress.json'
import * as routesConfig from '../../config/routesToWait.json'
import { Generate } from 'fixtures/generate'

describe('Add task', () => {
    context('1', () => {
        it('validate adding task', () => {
            cy.get(homepage.addTaskInput).type(`${tasks.defaultTask}{enter}`)
            cy.get(tasksComponents.allTasks)
                .find(tasksComponents.taskName)
                .should('contain.text', tasks.defaultTask)
        })
    })
    context('2', () => {
        it('validates removing task', () => {
            cy.get(homepage.addTaskInput).type(`${tasks.hebrewTask}{enter}`)
            cy.get(tasksComponents.taskName)
                .invoke('hover')
                .get(tasksComponents.removeTask)
                .first()
                .click()
            cy.get(tasksComponents.allTasks).should(
                'not.have.text',
                tasks.hebrewTask
            )
        })
    })
    context('3', () => {
        it('validates editing task', () => {
            cy.get(homepage.addTaskText).type(`${tasks.hebrewTask}{enter}`)
            cy.get(tasksComponents.taskName)
                .invoke('hover')
                .get(tasksComponents.editTask)
                .first()
                .click()
            cy.get(tasksComponents.editTaskInput).type(
                `{selectall}{backspace}${tasks.defaultTask}{enter}`
            )
            cy.get(tasksComponents.taskName).should(
                'have.text',
                tasks.defaultTask
            )
        })
    })
    context('4', () => {
        it('changes task status to completed', () => {
            cy.get(homepage.addTaskInput).type(`${tasks.defaultTask} {enter}`)
            cy.get(tasksComponents.allTasks)
                .find(tasksComponents.taskName)
                .click()
            cy.get(tasksComponents.taskName)
                .contains(tasks.defaultTask)
                .should(
                    'have.css',
                    'text-decoration',
                    'line-through solid rgb(189, 195, 199)'
                )
        })
    })
    context('5', () => {
        it('changes task status to uncompleted ', () => {
            cy.get(homepage.addTaskInput).type(`${tasks.defaultTask} {enter}`)
            cy.get(tasksComponents.allTasks)
                .find(tasksComponents.taskName)
                .dblclick()
            cy.get(tasksComponents.taskName).should(
                'have.css',
                'color',
                'rgb(52, 73, 94)'
            )
        })
    })
    context('6', () => {
        it('updates task date', () => {
            cy.get(homepage.addTaskInput).type(`${tasks.defaultTask}{enter}`)
            cy.get(tasksComponents.dateInput).type(
                `{selectall}{backspace} 18032001`
            )
            cy.get(tasksComponents.dateInput).should('have.text', '12/03/2001')
        })
    })
    context('7', () => {
        it('checks delete date changes to default', () => {
            const deleteDateRequest = 'deleteDateRequest'
            cy.get(homepage.addTaskInput).type(`${tasks.defaultTask}{enter}`)
            cy.intercept(
                'DELETE',
                new RegExp(
                    `.*/${cyConfig.todolistRoutes.task}${routesConfig.tasks.deleteDate}.*`
                )
            ).as(deleteDateRequest)
            cy.get(tasksComponents.resetDateButton)
                .click()
                .wait(`@${deleteDateRequest}`)
                .should((xhr) => {
                    expect(xhr.response.statusCode).to.equal(200)
                })
            cy.get(tasksComponents.dateInput).should('have.value', '18/08/2020')
        })
    })
    context('8', () => {
        it('adds list', () => {
            const listName = Generate.string('generatedList - ')
            cy.get(homepage.addTodoListInput)
                .type(`${listName}{enter}`)
                .get(homepage.showAllListsButton)
                .click()
                .get(tasksComponents.allLists)
                .should('contain.text', listName)
        })
    })
    context('9', () => {
        it('checks cancel deleting list cancels', () => {
            cy.get(homepage.addTodoListInput).type('list to cancel{enter}')
            cy.get(homepage.showAllListsButton).click()
            cy.get(homepage.removeListButton('list to cancel'))
                .click()
                .get(homepage.cancelRemoveList)
                .click()
            cy.get(tasksComponents.allLists).should(
                'have.text',
                'list to cancel'
            )
        })
    })
    context('10', () => {
        it('checks deleting list deletes', () => {
            cy.get(homepage.addTodoListInput).type('list to remove{enter}')
            cy.get(homepage.showAllListsButton).click()
            cy.get(homepage.removeListButton('list to remove'))
                .click()
                .get(homepage.approveRemoveList)
                .click()
            cy.get(tasksComponents.allLists).should(
                'not.have.text',
                'list to remove'
            )
        })
    })
})
