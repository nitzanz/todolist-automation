import { getPlaceholder, getSelector } from '../../fixtures/utils'

export const addTaskInput = getSelector('addTaskInput')
export const form = getSelector('form')
export const showAllListsButton = getSelector('openButton')
export const addTaskText = getPlaceholder('Add your task here...')
export const addListText = getPlaceholder('Add your task list here...')
export const addTodoListInput = getSelector('addTaskListInput')
export const listButton = `${getSelector('list')}`
export const getListSelector = (listName: string) => {
    return getSelector(`list ${listName}`)
}
export const removeListButton = (listName: string) =>
    getSelector(`removeIcon ${listName}`)
export const cancelRemoveList = getSelector('noBut')
export const approveRemoveList = getSelector('yesBut')
