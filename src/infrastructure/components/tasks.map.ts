import { getSelector } from '../../fixtures/utils'

export const allTasks = getSelector('TodoContainer')
export const taskName = getSelector('TodoLine')
export const allLists = getSelector('MainContainer')
export const removeTask = getSelector('removeIcon')
export const editTask = getSelector('editIcon')
export const editTaskInput = getSelector('editTaskInput')
export const dateInput = '#date-picker-inline'
export const resetDateButton = getSelector('removeDateButton')
