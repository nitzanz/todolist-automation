import axios from 'axios'
import * as cyconfig from '../../config/cypress.json'
import { ITodoList } from './interfaces'

export const createTodoList = (name: string) => {
    axios.post(
        `${cyconfig.todolistRoutes.main}${cyconfig.todolistRoutes.list}`,
        { name: name }
    )
}

export const deleteList = (listId: string) => {
    axios.delete(
        `${cyconfig.todolistRoutes.main}${cyconfig.todolistRoutes.list}delete/${listId}`
    )
}

export const getAllLists = async (): Promise<ITodoList[]> => {
    try {
        const response = await axios.get(
            `${cyconfig.todolistRoutes.main}${cyconfig.todolistRoutes.list}`
        )
        return response.data
    } catch (error) {
        return error.request.status
    }
}
