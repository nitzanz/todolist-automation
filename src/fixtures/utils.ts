import { StringIterator } from 'cypress/types/lodash'
import { deleteList, getAllLists } from './APICalls'
import { ITodoList } from './interfaces'

export const getSelector = (dataTestString: string) => {
    return `[data-test=${dataTestString}]`
}

export const getPlaceholder = (placeHolderText: string) => {
    return `[placeholder="${placeHolderText}"]`
}

export const mapIds = (allLists: ITodoList[]): string[] => {
    return allLists.map((list) => list.listId)
}

export const deleteAllLists = async () => {
    const allLists = await getAllLists()
    const listIds = mapIds(allLists)
    listIds.forEach((id) => {
        deleteList(id)
    })
}
