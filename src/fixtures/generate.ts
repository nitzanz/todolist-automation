import * as calls from './APICalls'

export namespace Generate {
    export const string = (prefix: string = '') => {
        return (
            prefix +
            Math.random()
                .toString(36)
                .replace(/[^a-z]+/g, '')
                .substr(0, 5)
        )
    }

    export const todoList = (name: string) => {
        calls.createTodoList(name)
    }
}
