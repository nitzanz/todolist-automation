import cyconfig from '../../config/cypress.json'
import {
    allLists,
} from '../infrastructure/components/tasks.map'
import { showAllListsButton } from '../infrastructure/pages/homepage.map'
import { Generate } from '../fixtures/generate'
import { deleteAllLists } from 'fixtures/utils'

Cypress.Commands.add(
    'createListAndEnter',
    { prevSubject: false },
    (name: string) => {
        Generate.todoList(name)
        cy.visit(cyconfig.baseUrl)
        cy.get(showAllListsButton).click()
        cy.get(allLists).contains(name).click().type('{esc}')
    }
)

Cypress.Commands.add(
    'deleteAllLists',
    { prevSubject: false },
    (listId: string) => {
        deleteAllLists()
    }
)
