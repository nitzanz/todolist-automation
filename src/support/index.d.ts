export {}
declare global {
    namespace Cypress {
        interface Chainable<Subject = any> {
            createListAndEnter(name: string): Chainable<void>
            async deleteAllLists(): Chainable<void>
        }
    }
}
