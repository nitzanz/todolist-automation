import { Generate } from 'fixtures/generate'
import './commands'

beforeEach(() => {
    cy.visit('/')
    cy.createListAndEnter(Generate.string())
})
afterEach(async () => {
    cy.deleteAllLists()
})
